## Desafio Terraform-Gitlab
Desafio proposto no Bootcamp Avanti DevOps, que consiste em: criar uma infraestrutura com o Terraform na AWS e criar um deploy com o GitLab na EC2.

## Criar uma infraestrutura com Terraform na AWS:
-  Criação dos arquivos de configuraação do Terraform, main.tf, ec2.tf, securitygroup.tf e script.sh;
- Aplicar os comandos terraform init, terraform plan, e terraform apply;
- Infraestrutura criada;

 ![EC2](./images/ec2.png)

## Criar um deploy com o GitLab na EC2:
- Criação de um repositório no GitLab contendo uma aplicação web simples;
- Vincular o servidor ao GitLab, de forma manual. Copiando o token do runner, colando na EC2 e especificando a descrição e as tags do runner;
- Criação do arquivo de pipeline .gitlab-ci.yml. Esse arquivo fará com que os commits realizados no repositório sejam alterados de forma automatizada no servidor;
- Subir o arquivo  de pipeline para o repositório no GitLab;
- Pipeline criado com sucesso;

 ![pipeline](./images/pipelines.png)

 ![website](./images/web-site.png)

